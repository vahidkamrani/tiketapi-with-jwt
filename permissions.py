from rest_framework import permissions
from tikets.models import Tiket



class IsOwnerOrReadOnly(permissions.BasePermission):
    message = 'permisson denied'
    def has_object_permission(self, request, view, obj):
        if obj.senderID == request.user or request.user.is_admin == True:
            if request.method in permissions.SAFE_METHODS:
                return True
            return obj.senderID == request.user
 
 


class IsOwnerOrReadOnlyExtra(permissions.BasePermission):
    message = 'permisson denied'
    def has_object_permission(self, request, view, obj):
        tikets = Tiket.objects.get(id=obj.id)
        receiverID_name=tikets.receiverID.all()
        A = list(receiverID_name)
        for i in range(len(A)):
            if A[i]== request.user or request.user.is_admin == True:
                if request.method in permissions.SAFE_METHODS:
                    return True
        return obj.receiverID == request.user
 