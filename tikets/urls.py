from django.urls import path
from . import views



app_name = 'tikets'

urlpatterns = [

    path('', views.TiketListView.as_view(), name='tikets_all'),
        #127.0.0.1:8000/tikets
    path('create', views.TiketCreateView.as_view(), name='tiket_create'),
        #127.0.0.1:8000/tikets/create 
    path('<int:pk>', views.TiketGetView.as_view(), name = 'tiket_get_senderid'),
        #127.0.0.1:8000/tikets/id
    path('recievedtiketslist/', views.RecievedTiketsListView.as_view(), name='tikets_get_receiverid'),
        #127.0.0.1:8000/tikets/recievedtiketslist
    path('senttiketslist/', views.SentTiketsListView.as_view(), name='tikets_get_receiverid'),
        #127.0.0.1:8000/tikets/senttiketslist
    path('seen/<int:pk>', views.TiketUpdateView.as_view(), name = 'tiket_get_senderid'),
        #127.0.0.1:8000/tikets/seen/id
     path('close/<int:pk>', views.TiketCloseUpdateView.as_view(), name = 'tiket_get_senderid'),
        #127.0.0.1:8000/tikets/close/id    
]