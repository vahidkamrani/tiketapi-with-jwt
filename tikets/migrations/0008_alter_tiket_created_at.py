
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tikets', '0007_remove_tiket_receiverid_tiket_receiverid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tiket',
            name='created_at',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
