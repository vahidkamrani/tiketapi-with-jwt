# Generated by Django 4.0.4 on 2022-05-21 09:42

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tikets', '0006_alter_tiket_created_at'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tiket',
            name='receiverID',
        ),
        migrations.AddField(
            model_name='tiket',
            name='receiverID',
            field=models.ManyToManyField(related_name='rtiket', to=settings.AUTH_USER_MODEL),
        ),
    ]
