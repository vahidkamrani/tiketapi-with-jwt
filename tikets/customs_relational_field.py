from rest_framework import serializers



class LastnameFristnameRelationalField(serializers.RelatedField):
    def to_representation(self, value):
        return f'{value.firstname} {value.lastname}'