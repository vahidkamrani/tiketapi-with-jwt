from django.apps import AppConfig


class TiketsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tikets'



    def ready(self):
        from . import signals



