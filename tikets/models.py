# from typing_extensions import Required
from django.db import models
from accounts.models import User
from extentions.utils import jalali_converter




class Tiket(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField(max_length=500)
    file = models.FileField(null=True, blank=True, upload_to='uploads/')
    created_at = models.DateField(null=True, blank=True, auto_now_add=True)
    senderID = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='stiket')
    receiverID = models.ManyToManyField(User, related_name='rtiket')

    STATUS_CHOICE = [
        ('بالا', 'بالا'),
        ('متوسط', 'متوسط'),
        ('پایین', 'پایین'),
    ]
    status = models.CharField(max_length=5, choices=STATUS_CHOICE)
    been_answer = models.BooleanField(default=False)
    closed = models.BooleanField(default=False)
    seen = models.BooleanField(default=False)
    expire_date = models.DateField()

    def __str__(self):
        return f'{self.title}'

    def jcreated_at(self):
        return jalali_converter(self.created_at)
    jcreated_at.short_description="تاریخ ارسال"
    
    def jexpire_date(self):
        return jalali_converter(self.expire_date)
    jexpire_date.short_description="مهلت پاسخ"
    
    


    