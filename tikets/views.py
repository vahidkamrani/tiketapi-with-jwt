
from urllib import response
from accounts.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404,get_list_or_404
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from permissions import IsOwnerOrReadOnly, IsOwnerOrReadOnlyExtra
from .models import Tiket
from .serializers import TiketSerializer, TiketListSerializer
from answertikets.serializers import AnswerTiketSerializer
from answertikets.models import AnswerTiket
from extentions.utils import miladi_converter
from django.db.models import Q
from itertools import chain
from operator import attrgetter



class TiketCreateView(APIView):
    """
        Create new tiket
    """
    permission_classes=[IsAuthenticated,]
    serializer_class = TiketSerializer
    
    def post(self, request):
        tiketobj=dict(request.data)
        try:
            data = {
                'title': request.data['title'],
                'content': request.data['content'],
                'status': request.data['status'],
                'senderID': request.user.id,
                'receiverID': tiketobj['receiverID']
            }
        except:
            return Response({"massage":"please enter your data"}, status=status.HTTP_404_NOT_FOUND)

        else:
            try:
                data['expire_date']=miladi_converter(request.data['expire_date'])

            except: 
                pass
            
            try:
                if tiketobj['file']==['']:
                    data['file']=None
                else:
                    data['file']=request.data['file']
            except:
                pass

            ser_data = self.serializer_class(data=data)

            if ser_data.is_valid():  
               
                ser_data.save()
                return Response(ser_data.data, status=status.HTTP_201_CREATED)
          
            return Response(ser_data.errors, status=status.HTTP_400_BAD_REQUEST)


class TiketUpdateView(APIView):
    """
        Update seen field of tikets and answers
    """
    permission_classes=[IsAuthenticated,]
    serializer_class = TiketSerializer

    def patch(self, request,pk, *args, **kwargs):
        user=request.user

        tiket_obj=Tiket.objects.filter( Q(pk=pk) & (Q(receiverID=user)))
        answers_list=AnswerTiket.objects.filter( Q(tiket_id=pk) & (Q(receiverID=user)))

        res=""
        for tiket in tiket_obj:
            tiket.seen=True
            tiket.save()
            res=res+str(tiket.id)+" "
           
        
        for answer in answers_list:
            answer.seen=True
            answer.save()
            res=res+str(answer.id)+" "


        print(res)
        if res=="":  
            return Response(status= status.HTTP_404_NOT_FOUND)

        return Response( status=status.HTTP_200_OK)


class TiketCloseUpdateView(APIView):
    """
        Update closed field of tikets and answers
    """
    permission_classes=[IsAuthenticated,]
    serializer_class = TiketSerializer

    def patch(self, request,pk, *args, **kwargs):
        user=request.user

        tiket_obj=Tiket.objects.filter( Q(pk=pk) & (Q(receiverID=user)))
        if tiket_obj:
            for tiket in tiket_obj:
                tiket.closed=True
                tiket.save()
                return Response( status=status.HTTP_200_OK)
        else:
            answers_list=AnswerTiket.objects.filter( Q(tiket_id=pk) & (Q(receiverID=user)))

            if  answers_list:
                tiket_obj=Tiket.objects.filter( Q(pk=pk) )

                print(answers_list)
                for tiket in tiket_obj:
                    tiket.closed=True
                    tiket.save()
                    return Response( status=status.HTTP_200_OK)

        return Response(status= status.HTTP_404_NOT_FOUND)



class TiketListView(APIView):
    """
        Get All tikets
    """
    permission_classes=[IsAuthenticated, IsAdminUser]
    serializer_class = TiketListSerializer
    def get(self, request):
        tikets = Tiket.objects.all()
        ser_data = self.serializer_class(tikets, many=True)
        return Response(ser_data.data, status=status.HTTP_200_OK)




# class TiketGetView(APIView):
#     """
#         Get tiket (senderid)
#     """
#     serializer_class = TiketListSerializer
#     permission_classes=[IsAuthenticated, IsOwnerOrReadOnly,]
    
#     def get(self, request, pk):
#         tiket = get_object_or_404(Tiket, pk=pk)
#         self.check_object_permissions(request, tiket)
#         ser_data = self.serializer_class(tiket)
#         return Response(ser_data.data, status=status.HTTP_200_OK)

class TiketGetView(APIView):
    """
        Get tiket (senderid)
    """
    serializer_class = TiketListSerializer
    
    def get(self, request, pk):
        user=request.user
        
        if user.is_admin == True:
            tiket = get_object_or_404(Tiket, pk=pk)
            ser_data = self.serializer_class(tiket)
            return Response(ser_data.data, status=status.HTTP_200_OK)

        else:
            # tiket=get_object_or_404(Tiket, Q(pk=pk) & ((Q(tanswertiket__receiverID=user))|(Q(receiverID=user))|(Q(tanswertiket__senderID=user))|(Q(senderID=user))))
            tiket=Tiket.objects.filter( Q(pk=pk) & ((Q(tanswertiket__receiverID=user))|(Q(receiverID=user))|(Q(tanswertiket__senderID=user))|(Q(senderID=user))))
            if tiket:
                ser_data = self.serializer_class(tiket[0])

                return Response(ser_data.data, status=status.HTTP_200_OK)
            else:
                return Response({'massage':'Not Found'}, status=status.HTTP_404_NOT_FOUND)


        
      


# class TiketGetViewExtra(APIView):
#     """
#         Get tiket (receiverid)
#     """
#     serializer_class = TiketListSerializer
#     permission_classes=[IsAuthenticated, IsOwnerOrReadOnlyExtra,]
    
#     def get(self, request, pk):
#         tiket = get_object_or_404(Tiket, pk=pk)
#         self.check_object_permissions(request, tiket)
#         ser_data = self.serializer_class(tiket)
#         return Response(ser_data.data, status=status.HTTP_200_OK)


class RecievedTiketsListView(APIView):
    """
        Get All Recieved Tikets for logined user
    """
    permission_classes=[IsAuthenticated, ]
    serializer_class = TiketListSerializer

    def get(self, request):
        user=request.user
        # tikets = Tiket.objects.filter(Q( Q(tanswertiket__receiverID=user) & Q(tanswertiket__been_answer=False) ) | (Q(receiverID=user) & Q(been_answer=False)))
        # tikets = Tiket.objects.raw('select * from tikets_tiket_receiverID,tikets_tiket,answertikets_answertiket,answertikets_answertiket_receiverID where tikets_tiket_receiverID.userid=')
        tikets1 = Tiket.objects.filter(Q( Q(tanswertiket__receiverID=user) & Q(tanswertiket__been_answer=False) & Q(closed=False)) )
        tikets2 = Tiket.objects.filter(Q(receiverID=user) & Q(been_answer=False)& Q(closed=False))
        tikets =sorted(list(chain(tikets1,tikets2)),key=attrgetter('id'))
        ser_data = self.serializer_class(tikets, many=True)
        return Response(ser_data.data, status=status.HTTP_200_OK)


class SentTiketsListView(APIView):
    """
        Get All Sent Tikets for logined user
    """
    permission_classes=[IsAuthenticated, ]
    serializer_class = TiketListSerializer

    def get(self, request):
        user=request.user
   
        tikets = Tiket.objects.filter(Q(tanswertiket__senderID=user) | (Q(senderID=user)))
 
        # tikets1 = Tiket.objects.filter((Q(senderID=user)))
        # tikets2 = Tiket.objects.filter((Q(tanswertiket__senderID=user)))
            
        # # tikets =sorted(list(chain(tikets1,tikets2)),key=attrgetter('id'))
        # tikets =tikets1|tikets2
        tikets = tikets.distinct().order_by('id')

        for t in tikets:
            print(t.id)
        ser_data = self.serializer_class(tikets, many=True)
        return Response(ser_data.data, status=status.HTTP_200_OK)