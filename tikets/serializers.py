from rest_framework import serializers
from .models import Tiket
from django.utils import timezone
from answertikets.serializers import AnswerTiketSerializer,AnswerDetailSerializer
from answertikets.models import AnswerTiket
from .customs_relational_field import LastnameFristnameRelationalField
from datetime import date
from accounts.serializers import UserSerializer
from extentions.utils import jalali_converter




class TiketSerializer(serializers.ModelSerializer):
    # used for posting a tiket

    class Meta:
        model = Tiket
        fields = "__all__"
        read_only_fields = ("created_at",)


    def create(self, validated_data):
        obj = super().create(validated_data)
        obj.created_at = date.today()
        obj.save()
        return obj



class TiketListSerializer(serializers.ModelSerializer):
    # used for getting tikets

    answer = serializers.SerializerMethodField()
    expire_date=serializers.SerializerMethodField()
    created_at=serializers.SerializerMethodField()
    receiverID=UserSerializer(many=True)
    senderID=UserSerializer()
    # receiverID = LastnameFristnameRelationalField(read_only=True)
    # senderID = LastnameFristnameRelationalField(read_only=True)

    class Meta:
        model = Tiket
        fields = "__all__"
        

    def get_answer(self, obj):
        result = obj.tanswertiket.all()
        return AnswerDetailSerializer(instance=result, many=True).data


    def get_expire_date(self,obj):
        result=jalali_converter(obj.expire_date)
        return result

    def get_created_at(self,obj):
        result=jalali_converter(obj.created_at)
        return result

