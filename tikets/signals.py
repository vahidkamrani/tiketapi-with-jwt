from django.core.signals import request_started
from django.db.models.signals import post_save
from answertikets.models import AnswerTiket
from .models import Tiket
from django.db.models import Q



def A(sender, **kwargs):
    user=kwargs["instance"].senderID.id
    if kwargs["created"]:
        a=kwargs["instance"].tiket_id
        b=a.id
        tiket_list=Tiket.objects.filter( Q(pk=b) & (Q(receiverID=user)))
        for tiket_obj in tiket_list:
            tiket_obj.been_answer = 'True'
            tiket_obj.save()

        answer_list=AnswerTiket.objects.filter( Q(tiket_id=b) & (Q(receiverID=user)))

        for answer in answer_list:
            answer.been_answer = "True"
            answer.save()


# def my_callback(sender, **kwargs):
#     print('start')
#     print(kwargs)
#     print("*"*30)
#     # a=kwargs["instance"].tiket_id
#     # b=a.id
#     # c = Tiket.objects.get(id=b)
#     # c.seen = 'True'
#     # c.save()

post_save.connect(receiver=A, sender=AnswerTiket )
# request_started.connect(my_callback)
