from . import jalali
from django.utils import timezone


def jalali_converter(time):
    # time=timezone.localtime(time)
    # time=timezone.localdate(time)
    # time_to_str="{}-{}-{}".format(time.year, time.month, time.day)
    # print(time)
    time_to_str=time
    time_to_tuple=jalali.Gregorian(time_to_str).persian_tuple()
    output = "{}-{}-{}".format(
        time_to_tuple[0],
        time_to_tuple[1],
        time_to_tuple[2],
    )
    return output

def miladi_converter(time):
    # time=timezone.localtime(time)
    # time=timezone.localdate(time)
    # time_to_str="{}-{}-{}".format(time.year, time.month, time.day)
    time_to_str=time
    time_to_tuple=jalali.Persian(time_to_str).gregorian_datetime()
    # output = "{}-{}-{}".format(
    #     time_to_tuple[0],
    #     time_to_tuple[1],
    #     time_to_tuple[2],
    # )
    output=time_to_tuple
    return output

def jalali_converter2(time):
    jmonths=[
    "فروردین","اردیبهشت","خرداد","تیر","مرداد","شهریور","مهر","آبان","آذر","دی","بهمن","اسفند",
    ]
    # time=timezone.localtime(time)

    # time_to_str="{}-{}-{}".format(time.year, time.month, time.day)
    time_to_str=time
    time_to_tuple=jalali.Gregorian(time_to_str).persian_tuple()

    time_to_list = list(time_to_tuple)
     
    for index,month in enumerate(jmonths):
        if time_to_list[1]== index+1:
            # print(index+1)
            time_to_list[1]=month
            # print(month)
            break


    output = "{}-{}-{}".format(
        time_to_list[0],
        time_to_list[1],
        time_to_list[2],
    )
    return output