FROM python:latest

WORKDIR /src

COPY packages.txt /src/

RUN pip install -U pip
RUN pip install -r packages.txt

COPY . /src/

EXPOSE 8000

CMD ["gunicorn", "A.wsgi", "8000"]
