from dataclasses import dataclass
from django.db.models import Count,Q
from tikets.models import Tiket
from answertikets.models import AnswerTiket
from accounts.models import User


@dataclass
class ReportReceiverEntry:
    receiverID: User
    count:int
    

@dataclass
class ReportSenderEntry:
    senderID:User
    count:int

def none_answered_tikets_report():
    data= []
    queryset = (Tiket.objects.filter(been_answer='False',closed='False')
            .values('receiverID')
            .annotate(tiketCount=Count('*'))
            .order_by('-tiketCount')[:5]
        )

    users_index={}
    for user in User.objects.all():
        users_index[user.pk]=user

    for entry in queryset:
        # receiverID= User.objects.get(pk=entry["receiverID"])
        receiverID= users_index.get(entry["receiverID"])
        report_entry=ReportReceiverEntry(receiverID,entry["tiketCount"])
        data.append(report_entry)
    return data

def none_answered_answers_report():
    data= []
    queryset = (AnswerTiket.objects.filter(Q(been_answer= 'True') &  Q( tiket_id__closed= False))
            .values('receiverID')
            .annotate(answerCount=Count('*'))
            .order_by('-answerCount')[:5]
        )

    users_index={}
    for user in User.objects.all():
        users_index[user.pk]=user

    for entry in queryset:
        # receiverID= User.objects.get(pk=entry["receiverID"])
        receiverID= users_index.get(entry["receiverID"])
        report_entry=ReportReceiverEntry(receiverID,entry["answerCount"])
        data.append(report_entry)
    return data


def sent_tikets_report():
    data= []
    queryset = (Tiket.objects.values('senderID')
            .annotate(tiketCount=Count('*'))
            .order_by('-tiketCount')[:5]
        )

    users_index={}
    for user in User.objects.all():
        users_index[user.pk]=user

    for entry in queryset:
        # receiverID= User.objects.get(pk=entry["receiverID"])
        senderID= users_index.get(entry["senderID"])
        report_entry=ReportSenderEntry(senderID,entry["tiketCount"])
        data.append(report_entry)
    return data


def received_tikets_report():
    data= []
    queryset = (Tiket.objects.values('receiverID')
            .annotate(tiketCount=Count('*'))
            .order_by('-tiketCount')[:5]
        )

    users_index={}
    for user in User.objects.all():
        users_index[user.pk]=user

    for entry in queryset:
        # receiverID= User.objects.get(pk=entry["receiverID"])
        receiverID= users_index.get(entry["receiverID"])
        report_entry=ReportReceiverEntry(receiverID,entry["tiketCount"])
        data.append(report_entry)
    return data


def sent_answers_report():
    data= []
    queryset = (AnswerTiket.objects.values('senderID')
            .annotate(answerCount=Count('*'))
            .order_by('-answerCount')[:5]
        )

    users_index={}
    for user in User.objects.all():
        users_index[user.pk]=user

    for entry in queryset:
        # receiverID= User.objects.get(pk=entry["receiverID"])
        senderID= users_index.get(entry["senderID"])
        report_entry=ReportSenderEntry(senderID,entry["answerCount"])
        data.append(report_entry)
    return data


def received_answers_report():
    data= []
    queryset = (AnswerTiket.objects.values('receiverID')
            .annotate(answerCount=Count('*'))
            .order_by('-answerCount')[:5]
        )

    users_index={}
    for user in User.objects.all():
        users_index[user.pk]=user

    for entry in queryset:
        # receiverID= User.objects.get(pk=entry["receiverID"])
        receiverID= users_index.get(entry["receiverID"])
        report_entry=ReportReceiverEntry(receiverID,entry["answerCount"])
        data.append(report_entry)
    return data





