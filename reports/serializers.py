from django.dispatch import receiver
from rest_framework import serializers
# from tikets.models import Tiket
from django.utils import timezone
# from answertikets.serializers import AnswerTiketSerializer
# from answertikets.models import AnswerTiket
from datetime import date
from accounts.serializers import UserSerializer


class ReportSerializer(serializers.Serializer):
    
    none_answered_tikets = serializers.IntegerField()
    none_answered_answers = serializers.IntegerField()
    sent_tikets = serializers.IntegerField()
    received_tikets = serializers.IntegerField()
    sent_answers = serializers.IntegerField()
    received_answers = serializers.IntegerField()


class StatusSerializer(serializers.Serializer):
    open_tikets = serializers.IntegerField()
    sent_tikets = serializers.IntegerField()
    answerd_tikets = serializers.IntegerField()
    closed_tikets = serializers.IntegerField()


class ReportReceiverEntrySerializer(serializers.Serializer):
    receiverID = UserSerializer()
    count= serializers.IntegerField()


class ReportSenderEntrySerializer(serializers.Serializer):
    senderID=UserSerializer()
    count= serializers.IntegerField()