from django.urls import path
from . import views



app_name = 'report'

urlpatterns = [
    path('generalreport', views.TicketReportsView.as_view(), name='general_report'),
        #127.0.0.1:8000/report/generalreport
    path('userreport', views.TicketStatusView.as_view(), name='user_report'),
        #127.0.0.1:8000/report/userreport  
   
]
