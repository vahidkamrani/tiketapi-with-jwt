from cgitb import reset
from unittest import result
from yaml import serialize
from tikets.models import Tiket
from answertikets.models import AnswerTiket
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import ReportSerializer,StatusSerializer,ReportReceiverEntrySerializer,ReportSenderEntrySerializer
from django.db.models import Q,Count
from .reports import none_answered_tikets_report, none_answered_answers_report, sent_tikets_report, received_tikets_report, sent_answers_report,received_answers_report
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from accounts.serializers import UserSerializer


class TicketStatusView(APIView):
    """
        Get summary of someones status tikets
    """
    serializer_class = StatusSerializer
    allowed_methods = ('get',)
    permission_classes=[IsAuthenticated]
    
    def get(self, request, *args, **kawrgs):
        user = self.request.user
        print(user)

        # open_tikets
        open_tiket= Tiket.objects.filter(closed='False', been_answer='False', receiverID=user).count()
        open_answer= AnswerTiket.objects.filter( Q( been_answer= 'False') & Q( tiket_id__closed= 'False') & Q(receiverID= user)).count()
   
        open_tikets= open_tiket+open_answer

        # sent_tikets
        sent_tiket= Tiket.objects.filter(senderID=user).count()
        sent_answer= AnswerTiket.objects.filter(senderID=user).count()
        
        sent_tikets= sent_tiket+ sent_answer
        
        # answerd_tikets
        answerd_tiket= Tiket.objects.filter(been_answer='True',receiverID=user).count()
        answerd_answertiket= AnswerTiket.objects.filter(been_answer='True',receiverID=user).count()
        
        answerd_tikets= answerd_tiket + answerd_answertiket

        # closed_tikets
        closed_tikets= Tiket.objects.filter( Q(closed= 'True') & ( Q( receiverID= user) | Q( senderID= user))).count()
       
        datat={
            "open_tikets": open_tikets, 
            "sent_tikets": sent_tikets,
            "answerd_tikets": answerd_tikets,
            "closed_tikets": closed_tikets,
        } 
        ser_data= StatusSerializer(datat)
        ser_user=UserSerializer(user)
        result={
            "status":ser_data.data,
            "user":ser_user.data
        }
        return Response(result, status=status.HTTP_200_OK)




class TicketReportsView(APIView):
    """
         in report page to get report a tikets' for admin
         Get report of tikets for admin
    """
    permission_classes=[IsAdminUser]

    def get(self, request):
        data0= none_answered_tikets_report()
        serializer0=ReportReceiverEntrySerializer(instance= data0, many=True)

        data1= none_answered_answers_report()
        serializer1=ReportReceiverEntrySerializer(instance= data1, many=True)

        data2= sent_tikets_report()
        serializer2=ReportSenderEntrySerializer(instance= data2, many=True)

        data3= received_tikets_report()
        serializer3=ReportReceiverEntrySerializer(instance= data3, many=True)

        data4= sent_answers_report()
        serializer4=ReportSenderEntrySerializer(instance= data4, many=True)

        data5= received_answers_report()
        serializer5=ReportReceiverEntrySerializer(instance= data5, many=True)
        data={
            "none_answered_tikets": serializer0.data,
            "none_answered_answers": serializer1.data,
            "sent_tikets": serializer2.data,
            "received_tikets": serializer3.data,
            "sent_answers": serializer4.data,
            "received_answers": serializer5.data
        }
        return Response(data, status=status.HTTP_200_OK)



# class TicketReportsView(APIView):
#     """
#         in report page to get report a tikets' for admin
#         Get report of tikets for admin
#     """
#     serializer_class = ReportSerializer

#     allowed_methods = ('get',)
    
#     def get(self, request, *args, **kawrgs):
#         # the most none-answered tikets
#         none_answered_tikets = (Tiket.objects.filter(been_answer='False',closed='False')
#             .values('receiverID')
#             .annotate(tiketCount=Count('*'))
#             .order_by('-tiketCount')[:5]
#         )
        
#         # the most none-answered answertikets
#         none_answered_answers = (AnswerTiket.objects.filter(Q(been_answer= 'True') &  Q( tiket_id__closed= False))
#             .values('receiverID')
#             .annotate(answerCount=Count('*'))
#             .order_by('-answerCount')[:5]
#         )

#         # the most sent tikets
#         sent_tikets= (Tiket.objects.values('senderID')
#             .annotate(tiketCount=Count('*'))
#             .order_by('-tiketCount')[:5]
#         )

#         # the most received tikets
#         received_tikets= (Tiket.objects.values('receiverID')
#             .annotate(tiketCount=Count('*'))
#             .order_by('-tiketCount')[:5]
#         )


#         # the most sent answertikets
#         sent_answers= (AnswerTiket.objects.values('senderID')
#             .annotate(answerCount=Count('*'))
#             .order_by('-answerCount')[:5]
#         )

#         # the most received answertikets
#         received_answers= (AnswerTiket.objects.values('receiverID')
#             .annotate(answerCount=Count('*'))
#             .order_by('-answerCount')[:5]
#         )

#         datat={
#             "none_answered_tikets": none_answered_tikets, 
#             "none_answered_answers": none_answered_answers,
#             "sent_tikets": sent_tikets,
#             "received_tikets": received_tikets,
#             "sent_answers": sent_answers,
#             "received_answers": received_answers
#         } 
#         ser_data= ReportSerializer(datat)
#         # return Response(ser_data.data, status=status.HTTP_200_OK)
#         return Response(datat, status=status.HTTP_200_OK)