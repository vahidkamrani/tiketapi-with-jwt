from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

urlpatterns = [
    path('admin/', admin.site.urls),
    path ('accounts/', include('accounts.urls', namespace='accounts')),
    path ('positions/', include('positions.urls', namespace='positions')),
    path ('answertikets/', include('answertikets.urls', namespace='answertikets')),
    path ('tikets/', include('tikets.urls', namespace='tikets')),
    path ('report/', include('reports.urls', namespace='report')),
    path ('', include('taskmanagement.urls',namespace='taskmanagement')),
    
    #Document API with Swagger
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path('schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),

    # Token Urls
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]