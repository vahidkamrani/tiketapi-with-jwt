from operator import imod
from django.urls import path
from . import views


app_name = 'positions'


urlpatterns = [

    #127.0.0.1:8000/positions/create
    path('create', views.PositionCreateView.as_view(), name='position_create'), 
    #127.0.0.1:8000/positions
    path('', views.PositionListView.as_view(), name='position_get'),  
]