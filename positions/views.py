from turtle import position
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Positions
from .serializers import PositionSerializer




class PositionCreateView(APIView):
    serializer_class = PositionSerializer

    def post(self, request):
        data = {
            'postname': request.data['postname']
        }
        ser_data = self.serializer_class(data=data)
        if ser_data.is_valid():
            Positions.objects.create(
                postname = ser_data.validated_data['postname'],
            )
            return Response(ser_data.data, status=status.HTTP_201_CREATED)
        return Response(ser_data.errors, status=status.HTTP_400_BAD_REQUEST)


class PositionListView(APIView):
    serializer_class = PositionSerializer
    def get(self, request):
        data = Positions.objects.all()
        ser_data = self.serializer_class(data, many=True)
        return Response(ser_data.data, status=status.HTTP_200_OK)
