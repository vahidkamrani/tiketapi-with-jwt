from django.urls import path
from . import views




app_name='accounts'
urlpatterns = [


    path('register', views.UserRegister.as_view(), name='register'), 
        #127.0.0.1:8000/accounts/register  
    path('login', views.UserLogin.as_view(), name='login'),
        #127.0.0.1:8000/accounts/login
    path('', views.UserList.as_view(), name='list'), 
        #127.0.0.1:8000/accounts/
    path('receivers', views.ReceiverList.as_view(), name='receivers'), 
        #127.0.0.1:8000/accounts/receivers
    path('<int:pk>', views.UserGet.as_view(), name='getuser'),
        #127.0.0.1:8000/accounts/{pk}
    path('update/<int:pk>', views.UserUpdate.as_view(), name='update'),
        #127.0.0.1:8000/accounts/update/{pk}
    path('delete/<int:pk>', views.UserDeactive.as_view(), name='update'),
        #127.0.0.1:8000/accounts/delete/{pk}
    path('change_password/', views.ChangePasswordView.as_view(), name='auth_change_password'),
        #127.0.0.1:8000/accounts/change_password/

]