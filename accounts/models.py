from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from accounts.manager import UserManager
from positions.models import Positions




class User(AbstractBaseUser):
    national_id = models.CharField(max_length=10, unique=True)
    post_name = models.ForeignKey(Positions, on_delete=models.CASCADE, related_name='pusers', null=True, blank=True)
    internal_phone = models.CharField(max_length=4, unique=True, blank=True, null=True)
    firstname = models.CharField(max_length=25)
    lastname = models.CharField(max_length=25)
    phonenumber = models.CharField(max_length=11, unique=True)
    head_name = models.ForeignKey('self', on_delete=models.CASCADE, related_name='husers', null= True, blank=True) 
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'national_id'
    REQUIRED_FIELDS = ['firstname', 'lastname', 'phonenumber']


    def __str__(self):
        return f'{self.firstname} {self.lastname}'
    
    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

