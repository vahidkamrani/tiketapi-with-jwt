from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserChangeForm, UserCreationForm
from .models import User



class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('national_id', 'firstname', 'lastname', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('national_id', 'password')}),
        ('Personal info', {'fields': ('firstname', 'lastname', 'phonenumber', 'head_name', 'internal_phone', 'post_name')}),
        ('Permissions', {'fields': ('is_admin', 'is_active', 'last_login')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('national_id', 'firstname', 'lastname', 'phonenumber', 'post_name', 'password1', 'password2'),
        }),
    )
    search_fields = ('national_id',)
    ordering = ('national_id',)
    filter_horizontal = ()


# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)
# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)