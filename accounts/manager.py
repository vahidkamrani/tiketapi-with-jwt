from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):

    def create_user(self, national_id, firstname, lastname, phonenumber, password):
        """
        Creates and saves a User with the given national_id, firstname, 
        lastname, phonenumber and password.
        """
        if not national_id:
            raise ValueError('Users must have an national_id')
        
        if not firstname:
            raise ValueError('Users must have an firstname')

        if not lastname:
            raise ValueError('Users must have an lastname')

        if not phonenumber:
            raise ValueError('Users must have an phonenumber')


        user = self.model(
           national_id = national_id,
            firstname = firstname,
            lastname = lastname,
            phonenumber = phonenumber,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user    


    def create_superuser(self, national_id, firstname, lastname, phonenumber, password):
        user = self.create_user(national_id, firstname, lastname, phonenumber, password)
        user.is_admin = True
        user.save(using=self._db)
        return user