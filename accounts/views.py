from urllib import request
from rest_framework.views import APIView
from .serializers import UserResigterSerializer, UserListSerializer, UserLoginSerializer,ChangePasswordSerializer,UserSerializer,UserUpdateSerializer
from rest_framework.response import Response
from rest_framework import status
from .models import User
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate, login
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from permissions import IsOwnerOrReadOnly
from rest_framework import generics
from rest_framework.generics import GenericAPIView

class UserRegister(APIView):
    """
        Create new user
    """
    serializer_class = UserResigterSerializer
    def post(self, request):
        ser_data = self.serializer_class(data=request.data)
    
        if ser_data.is_valid():
            ser_data.create(ser_data.validated_data)
            return Response(ser_data.data, status=status.HTTP_201_CREATED)
        return Response (ser_data.errors, status=status.HTTP_400_BAD_REQUEST)


class UserLogin(GenericAPIView):
    """
        Login User
    """
    serializer_class = UserLoginSerializer
    def post(self, request):
        national_id = request.data.get('national_id', None)
        password =  request.data.get('password', None)
        
        user = authenticate(username= national_id, password= password)

        if user:

            ser_data = self.serializer_class(user)
            return Response(ser_data.data, status=status.HTTP_200_OK)
        return Response({"massage":"User not found"})

        # ser_data = self.serializer_class(data=request.data)
        # if ser_data.is_valid():
        #     cd = ser_data.validated_data
        #     user = authenticate(request, national_id=cd['national_id'], password=cd['password'])
        #     if user is not None:
                
        #         return Response(ser_data.data, status=status.HTTP_200_OK)
        # return Response({"massage":"User not found"})


class UserList(APIView):
    """
        Get All Users
    """
    permission_classes=[IsAdminUser]
    serializer_class = UserListSerializer
    def get(self, request):
        users = User.objects.all()
        ser_data = self.serializer_class(instance=users, many=True)
        return Response(ser_data.data)


class ReceiverList(APIView):
    """
        Get All Receivers
    """
    permission_classes=[IsAuthenticated]
    serializer_class = UserListSerializer
    def get(self, request):
        users = User.objects.filter(is_active=True)
        ser_data = self.serializer_class(instance=users, many=True)
        return Response(ser_data.data)

class UserGet(APIView):
    """
        Get User
    """
    permission_classes=[IsAuthenticated]
    serializer_class = UserSerializer
    def get(self, request, pk):
        user = get_object_or_404(User, pk=pk)
        ser_data = self.serializer_class(instance=user)
        return Response(ser_data.data, status=status.HTTP_200_OK)


class UserUpdate(APIView):
    """
        Update User
    """
    permission_classes=[IsAuthenticated, IsOwnerOrReadOnly]
    serializer_class = UserUpdateSerializer
    def put(self, request, pk):
        user = User.objects.filter(pk=pk)
        if user:
            user = get_object_or_404(User, pk=pk)
        else:
            return Response( {'massage':'Not Found!'}, status=status.HTTP_404_NOT_FOUND)
        ser_data = self.serializer_class(instance=user, data=request.data, partial=True)
        if ser_data.is_valid():
            ser_data.save()
            return Response(ser_data.data, status=status.HTTP_200_OK)
        return Response(ser_data.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDeactive(APIView):
    """
        Deactive User
    """
    permission_classes=[IsAuthenticated]
    def delete(self, request, pk):
        user = get_object_or_404(User, pk=pk)
        user.is_active = False
        user.save()
        return Response({'massage':'This User is Deactive'})


class ChangePasswordView(generics.UpdateAPIView):
    """
        Change User's Password
    """
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer
    
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(pk=self.request.user.id)
        self.check_object_permissions(self.request, obj)
        return obj