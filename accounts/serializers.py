from urllib import request
from rest_framework import serializers
from .models import User
from positions.serializers import PositionSerializer
from positions.models import Positions




class UserResigterSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(required=True, write_only=True)
    class Meta:
        model = User
        fields = ('national_id', 'firstname' , 'lastname', 'phonenumber', 'password', 'password2','post_name')
        extra_kwargs = {
            'password': {'write_only':True}
        }
    def create(self, validated_data):
        del validated_data['password2']

        User.objects.create(national_id=validated_data['national_id'], firstname=validated_data['firstname'], 
                            lastname=validated_data['lastname'], phonenumber=validated_data['phonenumber'],
                            password=validated_data['password'], post_name=validated_data['post_name'])
     


    def validate(self, data):
        if data['password'] != data['password2']:
            raise serializers.ValidationError('password not match')
        return data



class UserListSerializer(serializers.ModelSerializer):
    post_name=PositionSerializer()
    class Meta:
        model = User

        fields = ('id', 'national_id', 'firstname' , 'lastname', 'phonenumber','post_name', 'is_active')



class UserLoginSerializer(serializers.ModelSerializer):
    
    password = serializers.CharField(write_only = True)
    class Meta:
        
        model = User
        fields = ('id', 'national_id', 'password', 'firstname', 'lastname','is_admin' ,'token' )

        read_only_fields = ['token']


class UserSerializer(serializers.ModelSerializer):
    post_name=PositionSerializer()
    class Meta:
        model = User
        fields = ('id', 'national_id', 'firstname' , 'lastname', 'phonenumber','post_name', 'is_active','is_admin')
        
class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'national_id', 'firstname' , 'lastname', 'phonenumber','post_name', 'is_active')
        



class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    password2 = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)
    
    class Meta:
        model = User
        fields = ('old_password', 'password', 'password2')
      
    def validate(self, attrs):
        user = self.context['request'].user

        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        elif  attrs['password'] == attrs['old_password']:
            raise serializers.ValidationError({"password": "Password and OldPassword fields cannot be same."})
        return attrs

    def validate_old_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError({"old_password": "Old password is not correct"})
        return value

    def update(self, instance, validated_data):
        instance=self.context['request'].user
        instance.set_password(validated_data['password'])
        instance.save()

        return instance
  


