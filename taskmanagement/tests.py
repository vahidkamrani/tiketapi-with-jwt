from urllib import response
from taskmanagement.models import Task
import pytest
from rest_framework.test import APIClient
from accounts.models import User 
from taskmanagement.serializers import UserSerializer
from rest_framework.test import APITestCase
from rest_framework import status
from django.test import Client, TestCase
from django.urls import reverse






    
# c = Client()

# @pytest.mark.django_db
# def test_taskmanagement():
#     response = c.post('/taskmanagement/', {'Title': 'john', 'Description': 'smith','user':'1'})
#     # response = c.get('/taskmanagement/')
#     print(response.content)
#     data = response.data 
#     print(data)
    # assert data["Title"] == 'john'
    # assert data["Description"] == 'smith'


# client = APIClient()


# @pytest.mark.django_db
# def test_taskmanagement():
#     # payload = dict(
#     #     Title= ['task 555'],
#     #     Description =[ "this is task 555"],
#     #     user =1,
       
#     # )
#     # payload=['task 55','this is task 55',1]
#     payload={
#     "Title" : "task 13",
#     "Description" : "this is task 13",
#     "user.pk": 1
# }
#     print(payload)
#     response = client.post('/taskmanagement/', payload)
#     print(response)
#     data = response.data 
#     print(data)
#     assert data["Title"] == payload["Title"]
#     assert data["Description"] == payload["Description"]


class TaskmanagementTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(national_id="1122334455",internal_phone="213",phonenumber ="423454", firstname="Test",lastname="test")

    def test_create(self):
        data = {
            "Title" : "task 55",
            "Description" : "this is task 55",
            "user" : 1
        }
        response = self.client.post('/taskmanagement/',data)
        print(response)
        # resdata=response.data
        # assert resdata["Title"] == data["Title"]
        # assert resdata["Description"] == data["Description"]
        self.assertEqual(response.status_code , status.HTTP_201_CREATED)
    
    def test_retrieve(self):
        response = self.client.get('/taskmanagement/',kwargs={"pk":'1'})
        print(response.data)
        # print('####')

        self.assertEqual(response.status_code , status.HTTP_200_OK)
        self.assertEqual(response.data["Title",],"task 55")



    # def test_destroy(self):
    #     data = {
    #         # "Title" : "task 55",
    #         # "Description" : "this is task 55",
    #         # "user" : "1"
    #         "id":3
    #     }
    #     response = self.client.delete('/taskmanagement/',data)
    #     self.assertEqual(response.status_code , status.HTTP_405_METHOD_NOT_ALLOWED)


