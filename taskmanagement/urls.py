from django.urls import path
from . import views 
from taskmanagement import views 

from rest_framework.routers import DefaultRouter

app_name = 'taskmanagement'

router = DefaultRouter()
router.register('taskmanagement', views.TaskViewSet)


urlpatterns = [
]

urlpatterns += router.urls