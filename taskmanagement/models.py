from django.db import models
from accounts.models import User


class Task(models.Model):
    title= models.CharField(max_length= 50)
    description= models.TextField(max_length= 500)
    created_at= models.DateField(null=True, blank=True, auto_now_add=True)

    user =  models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.title}'