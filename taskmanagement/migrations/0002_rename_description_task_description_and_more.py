# Generated by Django 4.0.4 on 2022-05-11 07:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('taskmanagement', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='Description',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='task',
            old_name='Title',
            new_name='title',
        ),
    ]
