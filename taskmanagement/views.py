import re
from rest_framework import status,viewsets,permissions
from .models import Task
from .serializers import TaskSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from permissions import IsOwnerOrReadOnly
from django.shortcuts import get_object_or_404


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class=TaskSerializer
    permission_classes=[IsAuthenticated,]
    # http_method_names = ['get','post','put','delete','patch']


    # list retrieved(get) all tasks
    def list(self, request, *args, **kwargs):
        # objs = super().list(request, *args, **kwargs)
        # print("----List----")
        # return objs
        tikets = Task.objects.filter(user= request.user.id)
        ser_data = self.serializer_class(tikets, many=True)
        return Response(ser_data.data, status=status.HTTP_200_OK)



    # create add(post) a new task 
    def create(self, request, *args, **kwargs):
        data = {
                'title': request.data['title'],
                'description': request.data['description'],
                'user': request.user.id
            }
        ser_data = self.serializer_class(data=data)
        if ser_data.is_valid():  
            ser_data.save()
            return Response(ser_data.data, status=status.HTTP_201_CREATED)
        return Response(ser_data.errors, status=status.HTTP_400_BAD_REQUEST)

       


    # changes the specific task
    # def update(self, request, *args, **kwargs):
    #     obj = super().update(request, *args, **kwargs)
    #     instance = self.get_object()
    #     print("----Update : {} ----".format(instance.title))
    #     return obj

    def update(self, request,pk, *args, **kwargs):
        task = Task.objects.filter(pk=pk,user=request.user)
        if task:
            task = get_object_or_404(Task, pk=pk)
        else:
            return Response( {'massage':'Not Found!'}, status=status.HTTP_404_NOT_FOUND)
        ser_data = self.serializer_class(instance=task, data=request.data, partial=True)
        if ser_data.is_valid():
            ser_data.save()
            return Response(ser_data.data, status=status.HTTP_200_OK)
        return Response(ser_data.errors, status=status.HTTP_400_BAD_REQUEST)

    # retrieve a specific task
    def retrieve(self, request,pk , *args, **kwargs):
        task = get_object_or_404(Task, pk=pk ,user= request.user.id)
        self.check_object_permissions(request, task)
        ser_data = self.serializer_class(task)
        return Response(ser_data.data, status=status.HTTP_200_OK)
    
    # delete a specific task
    def destroy(self, request, pk, *args, **kwargs):
        task = get_object_or_404(Task, pk=pk ,user= request.user)
        try:
            ser_task=self.serializer_class(task)
            task.delete()
            return Response(ser_task.data,status=status.HTTP_200_OK)
    
        except:
            print("******")
            return Response(status=status.HTTP_204_NO_CONTENT)
    
    def get_serializer_class(self):
        return super().get_serializer_class()
