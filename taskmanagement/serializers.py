from rest_framework import serializers
from .models import Task
from extentions.utils import jalali_converter
from accounts.models import User



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields= '__all__'


class TaskSerializer(serializers.ModelSerializer):
    user = UserSerializer
    created_at=serializers.SerializerMethodField()
    class Meta:
        model = Task
        fields = "__all__"
        read_only_fields = ("created_at",)

    def get_created_at(self,obj):
        result=jalali_converter(obj.created_at)
        return result



   