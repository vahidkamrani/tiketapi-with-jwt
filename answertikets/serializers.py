from rest_framework import serializers
from .models import AnswerTiket
from django.utils import timezone
from tikets.models import Tiket
from accounts.serializers import UserSerializer
from extentions.utils import jalali_converter
from datetime import date




class AnswerTiketSerializer(serializers.ModelSerializer):
    # used for posting an answer
    class Meta:
        model = AnswerTiket
        fields = "__all__"
        read_only_fields = ("created_at",)
        



    def create(self, validated_data):
        obj = super().create(validated_data)
        obj.created_at = date.today()
        obj.save()
        return obj


class AnswerDetailSerializer(serializers.ModelSerializer):
    # used for getting answers
    expire_date=serializers.SerializerMethodField()
    created_time=serializers.SerializerMethodField()
    receiverID=UserSerializer(many=True)
    senderID=UserSerializer()
    class Meta:
        model = AnswerTiket
        fields = "__all__"
        read_only_fields = ("created_at",)
        



    def create(self, validated_data):
        obj = super().create(validated_data)
        obj.created_at = date.today()
        obj.save()
        return obj

    def get_expire_date(self,obj):
        result=jalali_converter(obj.expire_date)
        return result

    def get_created_time(self,obj):
        result=jalali_converter(obj.created_time)
        return result
