
from rest_framework.response import Response
from .serializers import AnswerTiketSerializer
from rest_framework.views import APIView
from rest_framework import status
from tikets.models import Tiket
from rest_framework.permissions import IsAuthenticated
from extentions.utils import miladi_converter
from answertikets.serializers import AnswerTiketSerializer,AnswerDetailSerializer



class AnswerTiketCreateView(APIView):
    serializer_class = AnswerTiketSerializer
    permission_classes=[IsAuthenticated]

    def post (self, request):

        answerobj=dict(request.data)
        try:
            data = {
                'tiket_id': request.data['tiket_id'],
                'content': request.data['content'],
                'senderID': request.user.id,
                'receiverID': answerobj['receiverID']
            }
        except:
            return Response({"massage":"please enter your data"}, status=status.HTTP_404_NOT_FOUND)

        else:
            try:
                data['expire_date']=miladi_converter(request.data['expire_date'])

            except: 
                pass
            
            try:
                if answerobj['file']==['']:
                    data['file']=None
                else:
                    data['file']=request.data['file']
            except:
                pass

            ser_data = self.serializer_class(data=data)

            if ser_data.is_valid():  
               
                ser_data.save()
                return Response(ser_data.data, status=status.HTTP_201_CREATED)
          
            return Response(ser_data.errors, status=status.HTTP_400_BAD_REQUEST)

