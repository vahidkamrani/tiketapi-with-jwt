from django.urls import path

from answertikets.views import AnswerTiketCreateView



app_name = "answertikets"

urlpatterns = [

    path('create', AnswerTiketCreateView.as_view(), name='answertiket_create')
    
]