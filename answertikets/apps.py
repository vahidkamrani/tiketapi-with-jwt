from django.apps import AppConfig


class AnswertiketsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'answertikets'
