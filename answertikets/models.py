from django.db import models
from tikets.models import Tiket
from accounts.models import User



class AnswerTiket(models.Model):
    tiket_id=models.ForeignKey(Tiket,on_delete=models.CASCADE, related_name='tanswertiket')
    content = models.TextField(max_length=500)
    file = models.FileField(upload_to = 'uploads/', blank=True, null=True)
    created_time = models.DateField(null=True, blank=True, auto_now_add=True)
    senderID = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sanswertiket', blank=True, null=True)
    receiverID = models.ManyToManyField(User, related_name='ranswertiket')
    seen = models.BooleanField(default=False)
    been_answer = models.BooleanField(default=False)
    expire_date = models.DateField()
    


    def __str__(self):
        return f'TiketID: {self.tiket_id}  --  ReceiverID: {self.receiverID}  --  SenderID: {self.senderID}'
    