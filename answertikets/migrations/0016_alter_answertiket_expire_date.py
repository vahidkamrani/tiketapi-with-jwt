# Generated by Django 4.0.4 on 2022-07-28 07:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('answertikets', '0015_alter_answertiket_expire_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answertiket',
            name='expire_date',
            field=models.DateField(),
        ),
    ]
