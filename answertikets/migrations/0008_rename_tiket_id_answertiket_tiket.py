# Generated by Django 4.0.4 on 2022-05-24 04:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('answertikets', '0007_answertiket_been_answer'),
    ]

    operations = [
        migrations.RenameField(
            model_name='answertiket',
            old_name='tiket_id',
            new_name='tiket',
        ),
    ]
