# Generated by Django 4.0.4 on 2022-05-25 07:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('answertikets', '0010_answertiket_answer'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answertiket',
            name='answer',
        ),
    ]
